import { MuliPage } from './app.po';

describe('muli App', function() {
  let page: MuliPage;

  beforeEach(() => {
    page = new MuliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
