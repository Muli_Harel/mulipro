import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

users = [
    {name:'Muli', email:'muli@mail.com'},
    {name:'Kety', email:'kety@mail.com'},
    {name:'Harel', email:'harel@mail.com'}
  ]
  constructor() { }

  ngOnInit() {
  }

}
